# Techluster

The new distributed backends for Techmino

## Structure Chart

[![Chart Preview](http://assets.processon.com/chart_image/615fd6410e3e747620f50b70.png)](https://www.processon.com/view/link/61dc57760e3e74415775c1ab)

## Working List

### Connect

```mermaid
graph TD;
    A-->B;
    A-->C;
    B-->D;
    C-->D;
```
