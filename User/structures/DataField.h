//
// Created by particleg on 2021/10/2.
//

#pragma once

namespace tech::structures {
    enum class DataField {
        kPublic,
        kProtected,
        kPrivate
    };
}
